#  Semesterprojekt

## API Documentation

```typescript
GET /salesman

Response Interface:

Array<{ name: string; department: string; sid: string }>
```

```typescript
GET /evaluation-records/:sid

Response Interface:

Array<{
    _id: string;
    salesmanId: string;
    year: string;
    remarks: string;
    bonus: number;
    department: string;
    isApprovedByCEO: boolean;
}>
```

````typescript
GET /evaluation-records/:sid/:evaluationRecordId

Response Interface:

{
    sid: string;
    year: string;
    id: string;
    department: string;
    orderEvaluation: {
        [productName: string]: {
            clientName: string;
            // the id of the order - this is the id of the order inside the OpenCRX System
            id: string;
            // the id of the internal OrderBonus model - equals null, if the kpi was not yet evaluated
            bonusId: string | null;
            clientRanking: number;
            quantity: number;
            bonus: number | null;
            comment: string | null;
        }[];
    };
    socialEvaluation: Array<{
        kpi: string;
        // the id of the internal KPI model.
        id: string;
        // the id of the internal KPIBonus model - equals null, if the kpi was not yet evaluated
        bonusId: string | null;
        targetValue: string;
        actualValue: string;
        bonus: number | null;
        comment: string | null;
    }>;
    remarks: string;
    bonus: number;
    isApprovedByCEO: boolean;
}
````

```typescript
// Updating an evaluation record

PUT /evaluation-records/:evaluationRecordId

Important: The evaluation record id inside the request body has to be the same, as the one provided in the path parameter

Request Interface: Partial<IEvaluationRecordDTO> // The IEvaluationRecordDTO is the same datastructure, that gets retrieved when asking for a single record.
```

```typescript
// create a new record.
POST /evaluation-records

Request Interface: {year: string, salesmanId: string, department: string}
Response Interface:
{
    _id: string;
    salesmanId: string;
    year: string;
    remarks: string;
    bonus: number;
    department: string;
    isApprovedByCEO: boolean;
}
```

```typescript
// deletes an evaluation record.
DELETE /evaluation-records/:evaluationRecordId
```

```typescript
GET /auth

Response Interface:
{
    username: string;
    _id: string;
    role: "HR" | "CEO" | "Sales"
}
```

```typescript
POST /register

Request Interface:
{
    username: string;
    password: string;
    department: "HR" | "CEO" | "Sales"
}

Response Interface:
{
    accessToken: string;
}
```

```typescript
POST /login

Request Interface:
{
    username: string;
    password: string;
}

Response Interface:
{
    accessToken: string;
}
```

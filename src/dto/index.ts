export interface IOrderEvaluationDTO {
    productName: string;
    clientName: string;
    // the id of the order - this is the id of the order inside the OpenCRX System
    id: string;
    // the id of the internal OrderBonus model - equals null, if the kpi was not yet evaluated
    bonusId: string | null;
    clientRanking: number;
    quantity: number;
    bonus: number | null;
    comment: string | null;
}

export interface ISocialEvaluationDTO {
    kpi: string;
    // the id of the internal KPI model.
    id: string;
    // the id of the internal KPIBonus model - equals null, if the kpi was not yet evaluated
    bonusId: string | null;
    targetValue: number;
    actualValue: number;
    bonus: number | null;
    comment: string | null;
}

export interface IEvaluationRecordDTO {
    sid: string;
    year: string;
    id: string;
    department: string;
    orderEvaluation: IOrderEvaluationDTO[];
    socialEvaluation: ISocialEvaluationDTO[];
    remarks: string;
    bonus: number;
    isApprovedByHR: boolean;
    isApprovedByCEO: boolean;
    isApprovedBySalesman: boolean;
}

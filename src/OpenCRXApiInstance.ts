import ApiInstance from "./ApiInstance";

export interface OpenCRXApiInstanceOptions {
    username: string;
    password: string;
    baseURL: string;
}
export default class OpenCRXApiInstance extends ApiInstance {
    private readonly options: OpenCRXApiInstanceOptions;

    constructor() {
        super(process.env.OPEN_CRX_BASEURL);
        this.options = {
            username: process.env.OPEN_CRX_USERNAME,
            baseURL: process.env.OPEN_CRX_BASEURL,
            password: process.env.OPEN_CRX_PASSWORD,
        };
    }

    async authorize(): Promise<string> {
        throw new Error("Should never happen");
    }

    async getInitialHeaders(): Promise<object> {
        const basicAuth = `Basic ${Buffer.from(`${this.options.username}:${this.options.password}`).toString(
            "base64",
        )}`;
        return {
            Authorization: basicAuth,
            Accept: "application/json",
        };
    }
}

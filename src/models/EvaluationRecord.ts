import * as mongoose from "mongoose";
import { Schema } from "mongoose";
import { IKPIBonus } from "./KPIBonus";
import { IOrderBonus } from "./OrderBonus";

export interface IEvaluationRecord {
    _id: string;
    salesmanId: string;
    year: string;
    remarks: string;
    bonus: number;
    department: string;
    orderBonus: Array<IOrderBonus>;
    kpiBonus: Array<IKPIBonus>;
    isApprovedByCEO: boolean;
    isApprovedBySalesman: boolean;
    isApprovedByHR: boolean;
}

const schema = new mongoose.Schema({
    salesmanId: String,
    year: String,
    remarks: String,
    bonus: Number,
    department: String,
    orderBonus: [{ type: Schema.Types.ObjectId, ref: "OrderBonus" }],
    kpiBonus: [{ type: Schema.Types.ObjectId, ref: "KPIBonus" }],
    isApprovedByCEO: Boolean,
    isApprovedBySalesman: Boolean,
    isApprovedByHR: Boolean,
});

const EvaluationRecord = mongoose.model("EvaluationRecord", schema);
export default EvaluationRecord;

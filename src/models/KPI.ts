import mongoose from "mongoose";

const schema = new mongoose.Schema({
    kpi: String,
    targetValue: String,
});

const KPI = mongoose.model("KPI", schema);
export default KPI;

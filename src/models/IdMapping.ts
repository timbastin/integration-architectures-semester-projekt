import * as mongoose from "mongoose";
import { Schema } from "mongoose";

export interface IdMappingRecord {
    orangeHRMId: string;
    openCRXId: string;
}
const schema = new mongoose.Schema({
    orangeHRMId: String,
    openCRXId: String,
    governmentId: String,
    evaluationRecords: [{ type: Schema.Types.ObjectId, ref: "EvaluationRecord" }],
});

const IdMapping = mongoose.model("IdMapping", schema);
export default IdMapping;

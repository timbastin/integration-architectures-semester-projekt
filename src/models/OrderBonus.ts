import * as mongoose from "mongoose";
import { Schema } from "mongoose";

export interface IOrderBonus {
    _id: string;
    _order: string;
    comment: string;
    bonus: number;
}
const schema = new mongoose.Schema({
    evaluationRecord: { type: Schema.Types.ObjectId, ref: "EvaluationRecord" },
    bonus: Number,
    comment: String,
    // the id of the sale.
    _order: { type: String, unique: true },
});

const OrderBonus = mongoose.model("OrderBonus", schema);
export default OrderBonus;

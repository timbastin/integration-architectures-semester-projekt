import * as mongoose from "mongoose";

export enum Department {
    HR = "HR",
    CEO = "CEO",
    Sales = "Sales",
}
export interface UserType {
    username: string;
    // gets added by mongoose automatically.
    id: string;
    department: Department;
    password: string;
    // we do not store the password in the user type.
}

const schema = new mongoose.Schema({
    username: String,
    password: String,
    department: String,
});

const User = mongoose.model("User", schema);
export default User;

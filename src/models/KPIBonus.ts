import * as mongoose from "mongoose";
import { Schema } from "mongoose";

export interface IKPIBonus {
    _id: string;
    _kpi: string;
    targetValue: number;
    actualValue: number;
    bonus: number;
    comment: string;
}

const schema = new mongoose.Schema({
    evaluationRecord: { type: Schema.Types.ObjectId, ref: "EvaluationRecord" },
    _kpi: String,
    targetValue: Number,
    actualValue: Number,
    bonus: Number,
    comment: String,
});

const KPIBonus = mongoose.model("KPIBonus", schema);
export default KPIBonus;

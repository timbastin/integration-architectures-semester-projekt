import "reflect-metadata";
import Middleware from "./Middleware";

export interface Clazz<T> {
    new (...params: any): T;
}

/**
 * Adds the injectionTargets to the "target" parameter injection list.
 *
 * We just use the "target" parameter as store, to introduce the injection targets to the Container.
 * The "target" parameter, has to be known to the Container - or one element in the chain.
 * @param {Function} target
 * @param {Clazz<any>} injectionTarget
 */
export function markAsInjectionTarget(target: Function, ...injectionTarget: Clazz<any>[]) {
    const existingChain: Clazz<any>[] = Reflect.getOwnMetadata("app:injectionChain", target) || [];
    existingChain.push(...injectionTarget);
    Reflect.defineMetadata("app:injectionChain", existingChain, target);
}

export const ApplyMiddleware = (...middleware: Clazz<Middleware>[]): ClassDecorator => (target: Function) => {
    Reflect.defineMetadata("app:middleware", middleware, target);
    markAsInjectionTarget(target, ...middleware);
};

export const Inject = (toInject: Clazz<any>) => (
    target: Function,
    propertyKey: string | symbol,
    parameterIndex: number,
) => {
    let existingInjections: { index: number; type: Clazz<any> }[] =
        Reflect.getOwnMetadata("app:inject", target, propertyKey) || [];
    existingInjections.push({ index: parameterIndex, type: toInject });
    markAsInjectionTarget(target, toInject);
    Reflect.defineMetadata("app:inject", existingInjections, target);
};

export const Get = (path = "", middleware: Clazz<Middleware>[] = []): MethodDecorator =>
    defineHttpHandler("get", path, middleware);

export const Post = (path = "", middleware: Clazz<Middleware>[] = []): MethodDecorator =>
    defineHttpHandler("post", path, middleware);

export const Put = (path = "", middleware: Clazz<Middleware>[] = []): MethodDecorator =>
    defineHttpHandler("put", path, middleware);

export const Delete = (path = "", middleware: Clazz<Middleware>[] = []): MethodDecorator =>
    defineHttpHandler("delete", path, middleware);

export const Controller = (path: string): ClassDecorator => (target: Function) => {
    Reflect.defineMetadata("app:path", path, target);
};

export interface Handler {
    path: string;
    method: string;
    middleware: Clazz<Middleware>[];
}

export interface HandlerMetadata {
    post: Handler[];
    get: Handler[];
    put: Handler[];
    patch: Handler[];
    delete: Handler[];
}

export const defineHttpHandler = (method: keyof HandlerMetadata, path: string, middleware: Clazz<Middleware>[]) => {
    return function (target: Function, propertyKey: string) {
        const existingHandlers: HandlerMetadata = Reflect.getOwnMetadata("app:handler", target) || {
            post: [],
            get: [],
            put: [],
            patch: [],
            delete: [],
        };

        existingHandlers[method].push({ path, method: propertyKey, middleware: middleware });
        Reflect.defineMetadata("app:handler", existingHandlers, target);
    };
};

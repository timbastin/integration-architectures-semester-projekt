import express, { Express } from "express";
import { Clazz, Handler, HandlerMetadata } from "./decorators";
import Application from "./Application";
import HttpException from "./HttpException";
import Middleware from "./Middleware";
import * as dotenv from "dotenv";

dotenv.config();

export default class Container {
    private singletonStore: Map<Clazz<any>, any>;
    private readonly providers: Map<Clazz<any>, any>;
    static createApplication(app: Application, express?: Express, test = false): Container {
        return new Container(app, express, test);
    }

    constructor(
        protected readonly app: Application,
        protected readonly express?: Express | undefined,
        protected readonly test = false,
    ) {
        this.providers = Container.extractProvider(app);
    }

    public get<T>(clazz: Clazz<any>): T {
        if (this.singletonStore.has(clazz)) {
            return this.singletonStore.get(clazz);
        }
        throw new Error(`${clazz} could not be found in singleton store.`);
    }

    overwriteProvider(instructions: { provide: Clazz<any>; use: Clazz<any> }) {
        this.providers.set(instructions.provide, instructions.use);
        return this;
    }

    async boot() {
        this.singletonStore = Container.initiationLoop(this.providers);
        if (this.express) {
            await this.initServer();
        }
        await this.fireApplicationBootstrappedEvent();
        if (this.express && !this.test) {
            await this.express.listen(this.app.port ?? 3000, () => console.log("Application started successfully."));
        }
        return this;
    }

    private static extractProvider(app: Application): Map<Clazz<any>, any> {
        let providers: Map<Clazz<any>, any> = new Map();
        // simple things without reflection first - loop through all already defined providers.
        (app.providers ?? []).concat(app.controller ?? []).forEach((provider) => {
            if ("provide" in provider) {
                providers.set(provider.provide, provider.use);
            } else {
                providers.set(provider, provider);
            }
        });

        // lets see, if we can find some more providers.
        let currentProviders: Map<Clazz<any>, any> = providers;
        do {
            const nextProviders: Map<Clazz<any>, any> = new Map();
            for (const [provide, use] of currentProviders.entries()) {
                const foundProviders = Reflect.getOwnMetadata("app:injectionChain", provide);
                if (foundProviders) {
                    foundProviders.forEach((provider: Clazz<any>) => {
                        nextProviders.set(provider, provider);
                    });
                }
            }
            currentProviders = nextProviders;
            providers = new Map([...currentProviders, ...providers]);
        } while (currentProviders.size > 0);
        // unique the array.
        return providers;
    }

    private async initServer() {
        await this.initializeGlobalMiddleware();
        await this.registerController();
    }

    private async fireApplicationBootstrappedEvent() {
        for (const value of this.singletonStore.values()) {
            if (value["onApplicationBootstrap"] && typeof value["onApplicationBootstrap"] === "function") {
                await value["onApplicationBootstrap"]();
            }
        }
    }

    private httpProxy(
        method: "post" | "get" | "put" | "patch" | "delete",
        fn: (req: express.Request) => any | Promise<any>,
    ) {
        return async function (req: express.Request, res: express.Response) {
            try {
                const responseData = await fn(req);
                res.status(method === "post" ? 201 : 200).send(responseData);
            } catch (e) {
                Container.errorHandler(e, req, res);
            }
        };
    }

    private static errorHandler(err: Error, req: express.Request, res: express.Response) {
        console.log(err);
        if (err instanceof HttpException) {
            res.status(err.status).send({ error: err.message });
        } else {
            res.status(500).send({ error: "Internal Server Error" });
        }
    }

    private async registerController() {
        for (const controller of this.app.controller) {
            const provide = "provide" in controller ? controller.provide : controller;
            const instance = this.singletonStore.get(provide);
            const basePath = Reflect.getMetadata("app:path", controller);
            if (!basePath) {
                throw new Error(`${provide.name} not decorated with the Controller decorator`);
            }
            const maybeMiddlewares = Reflect.getMetadata("app:middleware", controller);
            if (maybeMiddlewares) {
                maybeMiddlewares.forEach((middlewareClass: Clazz<Middleware>) => {
                    const middlewareInstance: Middleware = this.singletonStore.get(middlewareClass);
                    this.express.use(basePath, middlewareInstance.handle.bind(middlewareInstance));
                });
            }
            const handlers: HandlerMetadata = Reflect.getMetadata("app:handler", instance);
            if (!handlers) {
                continue;
            }

            Object.keys(handlers).forEach((methodName: keyof HandlerMetadata) => {
                handlers[methodName].forEach((handler: Handler) => {
                    const completePath = basePath + handler.path;
                    // we can register some global exception handler right here.
                    // @ts-ignore
                    this.express[methodName](
                        completePath,
                        handler.middleware.map((middleware) => {
                            const instance: Middleware = this.singletonStore.get(middleware)!;
                            return instance.handle.bind(instance);
                        }),
                        this.httpProxy(methodName, instance[handler.method].bind(instance)),
                    );
                    if (!this.test)
                        console.log(
                            `Registered: ${provide.name}@${
                                instance[handler.method].name
                            } to path: ${methodName.toUpperCase()} ${completePath}`,
                        );
                });
            });
        }
    }

    private async initializeGlobalMiddleware() {
        this.app.globalMiddlewares.forEach((fn) => this.express.use(fn));
    }

    public init<T>(clazz: Clazz<T>): T {
        const necessaryInjections: { index: number; type: Clazz<any> }[] = Reflect.getMetadata("app:inject", clazz);
        if (!necessaryInjections || necessaryInjections.length === 0) {
            return new clazz();
        }

        const instances = necessaryInjections
            .sort((a, b) => a.index - b.index)
            .map(({ type }) => this.singletonStore.get(type));
        if (instances.some((inst) => inst === undefined)) {
            throw new Error("Instance does need unregistered instances in singleton store");
        }
        return new clazz(...instances);
    }

    /**
     * The initiation loop takes in all classes, and tries to initiate those.
     * Dependencies are getting injected.
     * @param {(Clazz<any> | {provide: Clazz<any>, use: any})[]} classes
     * @returns {Map<Clazz<any>, any>}
     * @private
     */
    private static initiationLoop(classes: Map<Clazz<any>, any>): Map<Clazz<any>, any> {
        const store = new Map<Clazz<any>, any>();
        let initiated = 0;
        do {
            initiated = 0;
            for (const [provide, use] of classes.entries()) {
                if (store.has(provide)) {
                    continue;
                }

                const necessaryInjections: { index: number; type: Clazz<any> }[] = Reflect.getMetadata(
                    "app:inject",
                    use,
                );
                if (!necessaryInjections || necessaryInjections.length === 0) {
                    // we can just initiate this class.
                    store.set(provide, new use());
                    initiated++;
                    continue;
                }

                // maybe we already initiated all providers for this subject.
                const instances = necessaryInjections
                    .sort((a, b) => a.index - b.index)
                    .map(({ type }) => store.get(type));
                if (!instances.some((inst) => inst === undefined)) {
                    // we can initiate this class.
                    store.set(provide, new use(...instances));
                    initiated++;
                }
                // it was not possible to initiate this class.
                // it depends on providers we did not already have initiated in the singleton store.
            }
        } while (initiated > 0);

        // the loop did finished. Lets have a look if we were able to initiate all of the subjects.
        if (store.size !== classes.size) {
            throw new Error("Could not initiate all subjects.");
        }
        return store;
    }
}

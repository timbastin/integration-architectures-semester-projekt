import e from "express";

export interface CrudController {
    delete(req: e.Request): Promise<any> | any;
    get(req: e.Request): Promise<any> | any;
    post(req: e.Request): Promise<any> | any;
    put(req: e.Request): Promise<any> | any;
}

import express from "express";

export default abstract class Middleware {
    /**
     * Should throw, if the execution should be stopped.
     * @param {e.Request} req
     * @param {e.Response} res
     * @param next
     * @returns {any}
     */
    abstract handle(req: express.Request, res: express.Response, next: express.NextFunction): any | Promise<any>;
}

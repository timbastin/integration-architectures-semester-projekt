import { Clazz } from "./decorators";
import { NextHandleFunction } from "connect";

export default interface Application {
    // they do not have to be crud controllers.
    controller?: (Clazz<any> | { provide: Clazz<any>; use: any })[];
    globalMiddlewares?: NextHandleFunction[];
    providers?: (Clazz<any> | { provide: Clazz<any>; use: any })[];
    port?: number;
}

import Middleware from "../lib/Middleware";
import { Inject } from "../lib/decorators";
import AuthService from "../services/AuthService";
import e from "express";
import HttpException from "../lib/HttpException";

export default class AuthMiddleware extends Middleware {
    constructor(@Inject(AuthService) protected readonly authService: AuthService) {
        super();
    }

    handle(req: e.Request, res: e.Response, next: e.NextFunction): any {
        const authHeader = req.header("Authorization");
        if (!authHeader) {
            throw new HttpException(401, "Please provide an access token");
        }
        const user = this.authService.verifyToken(authHeader?.substring(7));
        if (!user) {
            // throw something forbidden
            throw new HttpException(401, "Token was not valid.");
        }
        // @ts-ignore
        req["user"] = user;
        next();
    }
}

import EvaluationRecordController from "./controller/EvaluationRecordController";
import bodyParser from "body-parser";
import "reflect-metadata";
import MongoDB from "./db/MongoDB";
import AuthController from "./controller/AuthController";
import Application from "./lib/Application";
import SalesmanController from "./controller/SalesmanController";
import cors from "cors";
import KPIController from "./controller/KPIController";

export const App: Application = {
    controller: [AuthController, EvaluationRecordController, SalesmanController, KPIController],
    globalMiddlewares: [bodyParser.json(), cors()],
    providers: [MongoDB],
    port: 3000,
};

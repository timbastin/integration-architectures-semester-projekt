import { Controller, Get, Inject, Post } from "../lib/decorators";
import express from "express";
import HttpException from "../lib/HttpException";
import AuthService from "../services/AuthService";
import UserService from "../services/UserService";
import AuthMiddleware from "../middleware/AuthMiddleware";

@Controller("/auth")
export default class AuthController {
    constructor(
        @Inject(AuthService) protected readonly authService: AuthService,
        @Inject(UserService) protected readonly userService: UserService,
    ) {}
    @Post("/login")
    async login(req: express.Request) {
        const user = await this.userService.findUserByUsername(req.body.username);

        if (!user || !(await this.authService.compareHash(req.body.password, user.password))) {
            throw new HttpException(403, "Could not find a user with this username");
        }
        return { accessToken: await this.authService.signToken(user) };
    }

    @Get("/", [AuthMiddleware])
    async syncState(req: express.Request) {
        // @ts-ignore
        return req.user;
    }

    @Post("/register")
    async register(req: express.Request) {
        // check if the email does already exist
        let user = await this.userService.findUserByUsername(req.body.username);
        if (user) {
            throw new HttpException(400, "Email is already registered");
        }
        if (!req.body.department) {
            throw new HttpException(400, "Please provide a department when registering a user");
        }
        if (!req.body.username) {
            throw new HttpException(400, "Please provide a username when registering a user");
        }
        if (!req.body.password) {
            throw new HttpException(400, "Please provide a password when registering a user");
        }
        user = await this.userService.createUser(
            req.body.username,
            await this.authService.hash(req.body.password),
            req.body.department,
        );
        return { accessToken: await this.authService.signToken(user) };
    }
}

import { ApplyMiddleware, Controller, Delete, Get, Inject, Post } from "../lib/decorators";
import e from "express";
import AuthMiddleware from "../middleware/AuthMiddleware";
import HttpException from "../lib/HttpException";
import KPIService from "../services/KPIService";

@ApplyMiddleware(AuthMiddleware)
@Controller("/kpi")
export default class KPIController {
    constructor(@Inject(KPIService) protected readonly kpiService: KPIService) {}

    @Delete("/:id")
    async delete(req: e.Request): Promise<void> {
        const { id } = req.params;
        if (!id) {
            throw new HttpException(400, "No id in path provided");
        }
        await this.kpiService.delete(id);
    }

    @Post()
    async post(req: e.Request) {
        const { targetValue, kpi } = req.body;
        if (!targetValue || !kpi) {
            throw new HttpException(400, "Please provide a target value and a name for the kpi");
        }
        return this.kpiService.create({ targetValue, kpi });
    }

    @Get("/")
    get(req: e.Request): any {
        // lets fetch all evaluation records, we have from this salesman.
        return this.kpiService.getAll();
    }
}

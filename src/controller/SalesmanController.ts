import { ApplyMiddleware, Controller, Get, Inject } from "../lib/decorators";
import e from "express";
import SalesmanService from "../services/SalesmanService";
import { Salesman } from "../services/OrangeHRMService";
import IdMappingService from "../services/IdMappingService";
import AuthMiddleware from "../middleware/AuthMiddleware";
import HttpException from "../lib/HttpException";

@ApplyMiddleware(AuthMiddleware)
@Controller("/salesman")
export default class SalesmanController {
    constructor(
        @Inject(SalesmanService) protected readonly salesmanService: SalesmanService,
        @Inject(IdMappingService) protected readonly idMappingService: IdMappingService,
    ) {}

    @Get("/:sid")
    async show(req: e.Request): Promise<Salesman> {
        const { sid } = req.params;
        if (!sid) {
            throw new HttpException(400, "No id in path provided");
        }
        const idMapping = await this.idMappingService.findOne(sid);
        return this.salesmanService.getOneSalesman(idMapping.orangeHRMId);
    }

    @Get("/")
    async get(): Promise<Salesman[]> {
        return this.salesmanService.getAllSalesman();
    }
}

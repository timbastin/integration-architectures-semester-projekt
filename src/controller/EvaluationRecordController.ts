import { ApplyMiddleware, Controller, Delete, Get, Inject, Post, Put } from "../lib/decorators";
import e from "express";
import EvaluationRecord from "../models/EvaluationRecord";
import { CrudController } from "../lib/CrudController";
import AuthMiddleware from "../middleware/AuthMiddleware";
import HttpException from "../lib/HttpException";
import EvaluationRecordService from "../services/EvaluationRecordService";
import OpenCRXService from "../services/OpenCRXService";
import IdMappingService from "../services/IdMappingService";
import OrderBonusService from "../services/OrderBonusService";
import KPIService from "../services/KPIService";
import { IEvaluationRecordDTO, IOrderEvaluationDTO } from "../dto";
import KPIBonusService from "../services/KPIBonusService";
import { flattenDeep } from "lodash";
import OrangeHRMService from "../services/OrangeHRMService";
import { IKPIBonus } from "../models/KPIBonus";
import { IOrderBonus } from "../models/OrderBonus";

@ApplyMiddleware(AuthMiddleware)
@Controller("/evaluation-records")
export default class EvaluationRecordController implements CrudController {
    constructor(
        @Inject(KPIBonusService) protected readonly kpiBonusService: KPIBonusService,
        @Inject(KPIService) protected readonly kpiService: KPIService,
        @Inject(OrderBonusService) protected readonly orderBonusService: OrderBonusService,
        @Inject(IdMappingService) protected readonly idMappingService: IdMappingService,
        @Inject(EvaluationRecordService) protected readonly evaluationRecordService: EvaluationRecordService,
        @Inject(OpenCRXService) protected readonly openCRXService: OpenCRXService,
        @Inject(OrangeHRMService) protected readonly orangeHRMService: OrangeHRMService,
    ) {}

    @Delete("/:id")
    async delete(req: e.Request): Promise<void> {
        const { id } = req.params;
        if (!id) {
            throw new HttpException(400, "No id in path provided");
        }
        await EvaluationRecord.deleteOne({ _id: id });
    }

    @Post()
    async post(req: e.Request) {
        const { year, salesmanId, department } = req.body;
        if (!year || !salesmanId || !department) {
            throw new HttpException(400, "year, salesmanId and department are required inside the request body");
        }
        const record = await this.evaluationRecordService.getByYear(salesmanId, year, department);
        if (record) {
            // record does already exist an cannot be recreated.
            throw new HttpException(409, "The record does already exist.");
        }

        return this.evaluationRecordService.createForYear(salesmanId, year, department);
    }

    @Get("/:governmentId/:evaluationRecordId")
    async show(req: e.Request): Promise<IEvaluationRecordDTO> {
        const { evaluationRecordId } = req.params;
        if (!evaluationRecordId) {
            throw new HttpException(400, "Please provide an evaluation record id inside the path parameters");
        }
        return this.showRecord(evaluationRecordId);
    }

    @Put("/:evaluationRecordId")
    async put(req: e.Request<{ evaluationRecordId: string }, IEvaluationRecordDTO, IEvaluationRecordDTO>) {
        const { evaluationRecordId } = req.params;
        const passedRecord = req.body;
        if (!evaluationRecordId || evaluationRecordId !== passedRecord.id) {
            throw new HttpException(400, "No id in path provided");
        }
        const record = await this.evaluationRecordService.findOneById(evaluationRecordId);
        if (!record) {
            throw new HttpException(404, "Record does not exist");
        }
        // lets calculate the difference between the passed record and the one we already have inside the database.
        const orderBonus = await Promise.all(
            passedRecord.orderEvaluation.map(async (order) => {
                if (order.bonusId === null && (order.bonus !== null || order.comment !== null)) {
                    // we have to create the bonus
                    return this.orderBonusService.create({
                        _order: order.id,
                        comment: order.comment,
                        bonus: OrderBonusService.calculateBonus(order.clientRanking, order.quantity),
                    });
                } else if (order.bonusId !== null) {
                    // we just update the bonus.
                    return this.orderBonusService.update(order.bonusId, {
                        bonus: OrderBonusService.calculateBonus(order.clientRanking, order.quantity),
                        comment: order.comment,
                    });
                }
            }),
        );

        const kpiBonus = (
            await Promise.all(
                passedRecord.socialEvaluation.map((socialEv) => {
                    if ((socialEv.bonusId === null && socialEv.actualValue !== null) || socialEv.comment !== null) {
                        // we have to create the bonus
                        return this.kpiBonusService.create({
                            _kpi: socialEv.id,
                            targetValue: socialEv.targetValue,
                            actualValue: socialEv.actualValue,
                            comment: socialEv.comment,
                            bonus: KPIBonusService.calculateBonus(socialEv.actualValue, socialEv.targetValue),
                        });
                    } else if (socialEv.bonusId !== null) {
                        // we just update the bonus
                        return this.kpiBonusService.update(socialEv.bonusId, {
                            bonus: KPIBonusService.calculateBonus(socialEv.actualValue, socialEv.targetValue),
                            comment: socialEv.comment,
                        });
                    }
                }),
            )
        ).filter((el) => el);
        const values = {
            bonus: passedRecord.bonus,
            remarks: passedRecord.remarks,
            orderBonus: (orderBonus as unknown) as IOrderBonus[],
            kpiBonus: (kpiBonus as unknown) as IKPIBonus[],
            isApprovedByHR: passedRecord.isApprovedByHR,
            isApprovedByCEO: passedRecord.isApprovedByCEO,
            isApprovedBySalesman: passedRecord.isApprovedBySalesman,
        };
        await this.evaluationRecordService.update(record._id, values);

        if (passedRecord.isApprovedByCEO === true) {
            let orangeHRMId = (await this.idMappingService.findOne(passedRecord.sid)).orangeHRMId;
            await this.orangeHRMService.addBonusSalary(orangeHRMId, passedRecord.bonus, passedRecord.year);
        }
        return this.showRecord(evaluationRecordId);
    }

    @Get("/:governmentId")
    get(req: e.Request): any {
        // we are expecting a salesman id.
        const { governmentId } = req.params;
        if (!governmentId) {
            throw new HttpException(400, "Please provide the government id of a salesman as a route parameter");
        }
        // lets fetch all evaluation records, we have from this salesman.
        return this.evaluationRecordService.getAllRecordsOfSalesman(governmentId);
    }

    protected async showRecord(id: string) {
        const record = await this.evaluationRecordService.findOneById(id);
        // lets fetch all kpis and maybe populate those with values from the existing evaluation record.
        const mapping = await this.idMappingService.findOne(record.salesmanId);
        const [kpis, orders] = await Promise.all([
            this.kpiService.getAll(),
            this.openCRXService.getOpenCRXAggregatedInformationForSalesman(mapping.openCRXId, record.year),
        ]);
        const orderEvaluation: IOrderEvaluationDTO[] = flattenDeep(
            Object.keys(orders).map((key) => {
                return orders[key].map((order) => {
                    const boni = record.orderBonus.find((b) => b._order === order.id);
                    return {
                        ...order,
                        productName: key,
                        id: order.id,
                        bonusId: boni?._id ?? null,
                        bonus: OrderBonusService.calculateBonus(order.clientRanking, order.quantity) ?? null,
                        comment: boni?.comment ?? null,
                    };
                });
            }),
        );
        return {
            ...record,
            id: record._id,
            orderEvaluation: orderEvaluation,
            sid: record.salesmanId,
            socialEvaluation: kpis.map((kpi) => {
                const boni = record.kpiBonus.find((k) => {
                    return k._kpi === kpi._id.toString();
                });
                return {
                    ...kpi,
                    id: kpi._id,
                    targetValue: +kpi?.targetValue ?? null,
                    actualValue: boni?.actualValue ?? null,
                    comment: boni?.comment ?? null,
                    bonusId: boni?._id ?? null,
                    bonus: boni?.bonus ?? null,
                };
            }),
        };
    }
}

import axios, { AxiosInstance, AxiosRequestConfig } from "axios";
import { OnApplicationBootstrap } from "./lib/OnApplicationBootstrap";

export default abstract class ApiInstance implements OnApplicationBootstrap {
    public httpClient: AxiosInstance;

    private alreadyTriedToAuthorize: boolean = false;

    abstract authorize(): Promise<string> | string;
    abstract getInitialHeaders(): Promise<AxiosRequestConfig> | AxiosRequestConfig;

    protected constructor(baseURL: string) {
        this.httpClient = axios.create({
            baseURL,
        });
    }

    private async rejectedInterceptor(error: any) {
        if ("response" in error) {
            if ((error.response?.status === 401 || error.response.status === 403) && !this.alreadyTriedToAuthorize) {
                this.alreadyTriedToAuthorize = true;
                const token = await this.authorize();
                return this.httpClient.request({
                    ...error.config,
                    headers: {
                        ...error.config.headers,
                        ...this.getInitialHeaders(),
                        Authorization: `Bearer ${token}`,
                    },
                });
            }
        }
        return Promise.reject(error);
    }

    async onApplicationBootstrap(): Promise<void> {
        this.httpClient.defaults.headers = await this.getInitialHeaders();
        this.httpClient.interceptors.response.use((val) => val, this.rejectedInterceptor.bind(this));
    }
}

import express from "express";
import Container from "./lib/Container";
import { App } from "./App";

const e = express();
const router = express.Router();

const swaggerUi = require("swagger-ui-express"),
    swaggerDocument = require("./swagger.json");

e.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
e.use("/api/v1", router);

new Container(App, e).boot();

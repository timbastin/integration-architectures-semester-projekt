import { Inject } from "../lib/decorators";
import OpenCRXService from "./OpenCRXService";
import OrangeHRMService from "./OrangeHRMService";
import EvaluationRecord, { IEvaluationRecord } from "../models/EvaluationRecord";
import IdMappingService from "./IdMappingService";

export default class EvaluationRecordService {
    constructor(
        @Inject(IdMappingService) protected readonly idMappingService: IdMappingService,
        @Inject(OpenCRXService) protected readonly openCRXService: OpenCRXService,
        @Inject(OrangeHRMService) protected readonly orangeHRMService: OrangeHRMService,
    ) {}

    public async getAllRecordsOfSalesman(salesmanId: string): Promise<IEvaluationRecord[]> {
        return await EvaluationRecord.find({ salesmanId: salesmanId }).lean().exec();
    }

    public async createForYear(salesmanId: string, year: string, department: string) {
        return await EvaluationRecord.create({
            salesmanId,
            year,
            department,
            isApprovedByHR: false,
            isApprovedByCEO: false,
            isApprovedBySalesman: false,
        });
    }

    public getByYear(salesmanId: string, year: string, department: string) {
        return EvaluationRecord.findOne({ salesmanId, year, department }).lean().exec();
    }

    public findOneById(id: string): Promise<IEvaluationRecord> {
        return EvaluationRecord.findById(id).populate("orderBonus").populate("kpiBonus").lean().exec();
    }

    public update(id: string, values: Partial<IEvaluationRecord>) {
        return EvaluationRecord.findOneAndUpdate({ _id: id }, values, { new: true });
    }
}

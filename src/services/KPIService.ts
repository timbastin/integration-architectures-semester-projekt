import KPI from "../models/KPI";

export default class KPIService {
    public create(values: { kpi: string; targetValue: number }) {
        return KPI.create(values);
    }

    public getAll(): Promise<{ kpi: string; targetValue: string; _id: string }[]> {
        return KPI.find().lean().exec();
    }

    public update(id: string, values: Partial<{ kpi: string; targetValue: string }>) {
        return KPI.findOneAndUpdate({ _id: id }, values, { new: true });
    }

    public delete(id: string) {
        return KPI.deleteOne({ _id: id });
    }
}

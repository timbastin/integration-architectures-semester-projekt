import OrderBonus, { IOrderBonus } from "../models/OrderBonus";

export default class OrderBonusService {
    public create(values: Omit<IOrderBonus, "_id">) {
        return OrderBonus.create(values);
    }

    findOneByRemoteId(remoteId: string) {
        return OrderBonus.findOne({ remoteId }).lean().exec();
    }

    public update(id: string, values: Partial<IOrderBonus>) {
        return OrderBonus.findOneAndUpdate({ _id: id }, values, { new: true });
    }

    public static calculateBonus(clientRanking: number, quantity: number): number {
        let bonus;
        switch (clientRanking) {
            case 1:
                bonus = 600;
                break;
            case 2:
                bonus = 400;
                break;
            case 3:
                bonus = 100;
                break;
            case 4:
                bonus = 50;
                break;
            default:
                bonus = 0;
        }

        if (quantity == 0) {
            bonus += 0;
        } else if (quantity < 10) {
            bonus += 50;
        } else if (quantity >= 10 || quantity <= 25) {
            bonus += 100;
        }
        return bonus;
    }
}

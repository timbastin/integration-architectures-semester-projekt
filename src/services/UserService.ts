import User, { Department, UserType } from "../models/User";

export default class UserService {
    async findUserByUsername(username: string): Promise<UserType> {
        return User.findOne({ username }).lean().exec();
    }

    async createUser(username: string, password: string, department: Department): Promise<UserType> {
        const user = await User.create({ username, password, department });
        return user.toObject() as UserType;
    }
}

import { Inject } from "../lib/decorators";
import OpenCRXApiInstance from "../OpenCRXApiInstance";
import moment from "moment";

interface Contact {
    "@type": string;
    "@href": string;
    "@version": string;
    jobTitle: string;
    firstName: string;
    preferredContactMethod: number;
    accessLevelDelete: number;
    gender: number;
    salutationCode: number;
    department: string;
    governmentId: number;
    preferredWrittenLanguage: number;
    accessLevelUpdate: number;
    fullName: string;
    nickName: string;
    doNotPhone: false;
    owningUser: {
        "@href": string;
        $: string;
    };
}

interface Sale {
    "@type": string;
    "@href": string;
    "@version": string;
    calcRule: {
        "@href": string;
        $: string;
    };
    totalTaxAmount: string;
    modifiedAt: string;
    totalAmountIncludingTax: string;
    createdAt: string;
    paymentTerms: string;
    totalSalesCommission: string;
    identity: string;
    owningUser: {
        "@href": string;
        $: string;
    };
    totalDiscountAmount: string;
    freightTerms: string;
    contractCurrency: string;
    isGift: false;
    accessLevelUpdate: string;
    totalAmount: string;
    contractLanguage: string;
    pricingRule: {
        "@href": string;
        $: string;
    };
    contractState: string;
    totalBaseAmount: string;
    customer: {
        "@href": string;
        $: string;
    };
    submitStatus: string;
    activeOn: string;
    pricingState: string;
    name: string;
    accessLevelDelete: string;
    accessLevelBrowse: string;
    priority: string;
    shippingMethod: string;
    salesRep: {
        "@href": string;
        $: string;
    };
    contractNumber: string;
}

interface Position {
    baseAmount: string;
    lineItemNumber: number;
    "@href": string;
    taxAmount: string;
    amount: string;
    productDescription: string;
    quantity: string;
    positionNumber: number;
    product: {
        "@href": string;
        $: string;
    };
    name: number;
    quantityBackOrdered: string;
    discountAmount: string;
    pricePerUnit: string;
}

interface Product {
    productState: number;
    itemNumber: number;
    productNumber: number;
    maxPositions: number;
    maxQuantity: string;
    description: string;
    minQuantity: string;
    name: string;
    "@href": string;
}

interface Client {
    "@type": string;
    "@href": string;
    "@version": string;
    fullName: string;
    identity: string;
    numberOfEmployeesCategory: number;
    industry: number;
    name: string;
    accountRating: number;
}

export interface OpenCRXAggregatedResponse {
    [productName: string]: {
        clientName: string;
        id: string;
        clientRanking: number;
        quantity: number;
    }[];
}

export default class OpenCRXService {
    constructor(@Inject(OpenCRXApiInstance) protected readonly openCRXApiInstance: OpenCRXApiInstance) {}

    public async getOpenCRXAggregatedInformationForSalesman(
        openCRXId: string,
        year: string,
    ): Promise<OpenCRXAggregatedResponse> {
        const sales = await this.getAllSalesOfSalesman(openCRXId, year);
        // get all the customers and map the sales array, to include the desired information
        const populatedSales = await Promise.all(
            sales.map(async (sale) => {
                const [client, positions] = await Promise.all([
                    this.getClient(sale.customer["@href"]),
                    this.getPositionsOfSale(sale["@href"]),
                ]);
                return {
                    ...sale,
                    client,
                    positions: await Promise.all(
                        positions.map(async (position) => ({
                            ...position,
                            product: await this.getProduct(position.product["@href"]),
                        })),
                    ),
                };
            }),
        );
        // now we have all information - lets build the aggregated response object.
        const openCRXAggregatedResponse: OpenCRXAggregatedResponse = {};
        populatedSales.forEach((sale) => {
            sale.positions.forEach((position) => {
                const existing = openCRXAggregatedResponse[position.product.name] || [];
                existing.push({
                    id: position["@href"].substring(position["@href"].lastIndexOf("/") + 1),
                    clientName: sale.client.name,
                    clientRanking: sale.client.accountRating,
                    quantity: +position.quantity,
                });
                openCRXAggregatedResponse[position.product.name] = existing;
            });
        });
        return openCRXAggregatedResponse;
    }

    public async getSalesmanFromOpenCRXByGovernmentId(employeeId: string): Promise<Contact | null> {
        const salesman: { objects: Contact[] } = (
            await this.openCRXApiInstance.httpClient.get(
                `/org.opencrx.kernel.account1/provider/CRX/segment/Standard/account`,
            )
        ).data;

        return salesman.objects.find((s) => {
            if ("governmentId" in s) {
                return s.governmentId === +employeeId;
            }
            return false;
        });
    }

    //all salesOrder of one salesman
    public async getAllSalesOfSalesman(openCRXId: string, year: string): Promise<Sale[]> {
        const res = (
            await this.openCRXApiInstance.httpClient.get(
                "/org.opencrx.kernel.contract1/provider/CRX/segment/Standard/salesOrder/",
            )
        ).data;
        const { objects } = res as { objects: Sale[] };

        return objects.filter((sale) => {
            if (sale.salesRep["@href"].endsWith(openCRXId)) {
                return moment(sale.activeOn).format("YYYY") === year.toString();
            }
            return false;
        });
    }

    public async getPositionsOfSale(saleHref: string): Promise<Position[]> {
        const res: { objects: Position[] } = (await this.openCRXApiInstance.httpClient.get(`${saleHref}/position`))
            .data;
        return res.objects;
    }

    public async getProduct(productHref: string): Promise<Product> {
        return (await this.openCRXApiInstance.httpClient.get(productHref)).data;
    }

    public async getClient(clientHref: string): Promise<Client> {
        return (await this.openCRXApiInstance.httpClient.get(clientHref)).data;
    }
}

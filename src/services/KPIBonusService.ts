import KPIBonus, { IKPIBonus } from "../models/KPIBonus";

export default class KPIBonusService {
    public create(values: Omit<IKPIBonus, "_id">) {
        return KPIBonus.create(values);
    }

    public update(id: string, values: Partial<IKPIBonus>) {
        return KPIBonus.findOneAndUpdate({ _id: id }, values, { new: true });
    }

    public static calculateBonus(actualValue: number, targetValue: number): number {
        if (actualValue > targetValue) {
            return 100;
        } else if (actualValue == targetValue) {
            return 50;
        } else if (Math.round(actualValue / targetValue) >= 0.75) {
            return 20;
        } else if (Math.round(actualValue / targetValue) >= 0.5) {
            return 10;
        } else if (Math.round(actualValue / targetValue) >= 0.25) {
            return 5;
        } else {
            return 0;
        }
    }
}

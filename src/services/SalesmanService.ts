import { Inject } from "../lib/decorators";
import IdMappingService from "./IdMappingService";
import OpenCRXService from "./OpenCRXService";
import OrangeHRMService from "./OrangeHRMService";

export default class SalesmanService {
    constructor(
        @Inject(IdMappingService) protected readonly idMappingService: IdMappingService,
        @Inject(OpenCRXService) protected readonly openCRXService: OpenCRXService,
        @Inject(OrangeHRMService) protected readonly orangeHRMService: OrangeHRMService,
    ) {}

    public async getOneSalesman(orangeHRMId: string) {
        return this.orangeHRMService.getOneSalesman(orangeHRMId);
    }

    public async getAllSalesman() {
        const orangeHRMSalesmen = await this.orangeHRMService.getAllSalesman();
        const salesman = await Promise.all(
            orangeHRMSalesmen.map(async (salesman) => {
                const em = await this.idMappingService.findOne(salesman.code);
                if (em) {
                    // the employee does already exist.
                    return salesman;
                }
                // we have to fetch the employee, from openCRX and save him in our database.
                const openCRXSalesman = await this.openCRXService.getSalesmanFromOpenCRXByGovernmentId(salesman.code);
                if (openCRXSalesman) {
                    // we can create the id mapping.
                    await this.idMappingService.create(
                        salesman.employeeId,
                        openCRXSalesman["@href"].substring(openCRXSalesman["@href"].lastIndexOf("/") + 1),
                        salesman.code,
                    );
                    return salesman;
                }
            }),
        );

        // filter for undefined values.
        return salesman.filter((s) => s);
    }
}

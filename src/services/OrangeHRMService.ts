import { Inject } from "../lib/decorators";
import OrangeHRMApiInstance from "../OrangeHRMApiInstance";

export interface Salesman {
    firstName: string;
    middleName: string;
    lastName: string;
    code: string;
    employeeId: string;
    fullName: string;
    status: any;
    dob: any;
    driversLicenseNumber: string;
    licenseExpiryDate: any;
    maritalStatus: string;
    gender: any;
    otherId: string;
    nationality: any;
    unit: string;
    jobTitle: string;
    supervisor: any;
}

export default class OrangeHRMService {
    constructor(@Inject(OrangeHRMApiInstance) protected readonly orangeHRMService: OrangeHRMApiInstance) {}
    /**
     * Returns all sales which are currently part of the OrangeHRM system.
     * @returns {Promise<Salesman[]>}
     */
    public async getAllSalesman(): Promise<Salesman[]> {
        return (await this.orangeHRMService.httpClient.get("/api/v1/employee/search")).data.data;
    }

    public async getOneSalesman(sid: string): Promise<Salesman> {
        return (await this.orangeHRMService.httpClient.get(`/api/v1/employee/${sid}`)).data.data;
    }

    public async addBonusSalary(orangeHRMId: string, bonus: number, year: string) {
        return (
            await this.orangeHRMService.httpClient.post(
                `/api/v1/employee/${orangeHRMId}/bonussalary?year=${year}&value=${bonus}`,
                {
                    value: bonus,
                    year,
                },
            )
        ).data;
    }
}

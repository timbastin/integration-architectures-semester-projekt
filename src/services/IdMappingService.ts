import IdMapping, { IdMappingRecord } from "../models/IdMapping";

export default class IdMappingService {
    findOne(governmentId: string): Promise<IdMappingRecord> {
        return IdMapping.findOne({ governmentId }).lean().exec();
    }

    create(orangeHRMId: string, openCRXId: string, governmentId: string) {
        return IdMapping.create({ orangeHRMId, openCRXId, governmentId });
    }

    delete(governmentId: string) {
        return IdMapping.deleteOne({ governmentId });
    }
}

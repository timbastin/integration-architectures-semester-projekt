import { OnApplicationBootstrap } from "../lib/OnApplicationBootstrap";
import jwt from "jsonwebtoken";
import { readFile } from "fs/promises";
import { join } from "path";
import { UserType } from "../models/User";
import bcrypt from "bcrypt";

export default class AuthService implements OnApplicationBootstrap {
    privateKey: string;
    publicKey: string;

    async signToken(user: Omit<UserType, "password">) {
        // lets make sure, that the password does really not exist on the user type.
        if ("password" in user) {
            delete (user as UserType).password;
        }
        return jwt.sign(user, this.privateKey, { algorithm: "RS256" });
    }

    verifyToken(token: string): Omit<UserType, "password"> {
        return jwt.verify(token, this.publicKey, { ignoreExpiration: true }) as Omit<UserType, "password">;
    }

    async hash(str: string): Promise<string> {
        return bcrypt.hash(str, 10);
    }

    async compareHash(plain: string, encrypted: string) {
        return bcrypt.compare(plain, encrypted);
    }

    async onApplicationBootstrap(): Promise<void> {
        this.privateKey = await readFile(join(__dirname, "..", "keys", "jwtRS256.key"), { encoding: "utf8" });
        this.publicKey = await readFile(join(__dirname, "..", "keys", "jwtRS256.key.pub"), { encoding: "utf8" });
    }
}

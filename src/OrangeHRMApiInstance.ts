import ApiInstance from "./ApiInstance";

export interface OrangeHRMApiInstanceOptions {
    username: string;
    password: string;
    clientSecret: string;
    clientId: string;
    issueTokenUrl: string;
    baseURL: string;
}
export default class OrangeHRMApiInstance extends ApiInstance {
    private readonly options: OrangeHRMApiInstanceOptions;

    constructor() {
        super(process.env.ORANGE_HRM_BASEURL);
        this.options = {
            username: process.env.ORANGE_HRM_USERNAME,
            baseURL: process.env.ORANGE_HRM_BASEURL,
            clientSecret: process.env.ORANGE_HRM_CLIENT_SECRET,
            clientId: process.env.ORANGE_HRM_CLIENT_ID,
            password: process.env.ORANGE_HRM_PASSWORD,
            issueTokenUrl: process.env.ORANGE_HRM_ISSUE_TOKEN_URL,
        };
    }

    async authorize(): Promise<string> {
        return (
            await this.httpClient.post(this.options.issueTokenUrl, {
                username: this.options.username,
                password: this.options.password,
                client_id: this.options.clientId,
                client_secret: this.options.clientSecret,
                grant_type: "password",
            })
        ).data.access_token;
    }

    async getInitialHeaders(): Promise<object> {
        return {
            Authorization: "Bearer " + (await this.authorize()),
        };
    }
}

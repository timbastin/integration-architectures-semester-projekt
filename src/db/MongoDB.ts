import mongoose, { Mongoose } from "mongoose";
import { OnApplicationBootstrap } from "../lib/OnApplicationBootstrap";

/**
 * Does handle the connection to a database.
 */
export default class MongoDB implements OnApplicationBootstrap {
    private connection: Mongoose;
    public async init(): Promise<Mongoose> {
        if (!this.connection) {
            this.connection = await mongoose.connect("mongodb://localhost:27017", {
                useUnifiedTopology: true,
                dbName: "ia",
                user: "ia",
                pass: "secret",
                useNewUrlParser: true,
                useFindAndModify: false,
            });
        }
        return this.connection;
    }

    async onApplicationBootstrap(): Promise<any> {
        await this.init();
    }
}

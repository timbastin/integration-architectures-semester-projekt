import request from "supertest";

export default class TestUtils {
    static email: string = "Avocadobaum";
    static password: string = "secret";

    static authHeader(token: { accessToken: string }): [string, string] {
        return ["Authorization", `Bearer ${token.accessToken}`];
    }

    constructor(protected readonly superTest: request.SuperTest<request.Test>) {}

    login(): Promise<{ accessToken: string }> {
        // create a user and login into the system
        return new Promise<{ accessToken: string }>((resolve, reject) => {
            this.superTest
                .post("/auth/login")
                .send({ email: TestUtils.email, password: TestUtils.password })
                .end((err, res) => {
                    if (res.status === 201) {
                        // login was successful
                        return resolve(res.body);
                    } else {
                        this.superTest
                            .post("/auth/register")
                            .send({ email: TestUtils.email, password: TestUtils.password })
                            .end((err, res) => {
                                if (res.status === 201) {
                                    // login was successful
                                    return resolve(res.body);
                                }
                                return reject();
                            });
                    }
                });
        });
    }
}

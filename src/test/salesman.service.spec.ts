import SalesmanService from "../services/SalesmanService";
import Container from "../lib/Container";
import OpenCRXServiceStub from "./e2e/stubs/OpenCRXServiceStub";
import OpenCRXService from "../services/OpenCRXService";
import OrangeHRMService from "../services/OrangeHRMService";
import OrangeHRMServiceStub from "./e2e/stubs/OrangeHRMServiceStub";
import IdMappingService from "../services/IdMappingService";
import IdMappingServiceStub from "./e2e/stubs/IdMappingServiceStub";

describe("Salesman service test suite", () => {
    let service: SalesmanService;

    let orangeHRMService: OrangeHRMServiceStub;
    let openCRXService: OpenCRXServiceStub;
    let idMappingService: IdMappingServiceStub;
    beforeAll(async () => {
        const module = await Container.createApplication({ providers: [SalesmanService] })
            .overwriteProvider({
                provide: OpenCRXService,
                use: OpenCRXServiceStub,
            })
            .overwriteProvider({
                provide: OrangeHRMService,
                use: OrangeHRMServiceStub,
            })
            .overwriteProvider({
                provide: IdMappingService,
                use: IdMappingServiceStub,
            })
            .boot();

        service = module.get(SalesmanService);
        openCRXService = module.get(OpenCRXService);
        orangeHRMService = module.get(OrangeHRMService);
        idMappingService = module.get(IdMappingService);
    });
    describe("should sync the open crx and orangeHRM data", () => {});
});

import Container from "../lib/Container";
import OpenCRXService from "../services/OpenCRXService";
import OpenCRXApiInstance from "../OpenCRXApiInstance";
import ApiInstanceStub from "./e2e/stubs/ApiInstanceStub";

describe("OpenCRX Service test suite", () => {
    let service: OpenCRXService;
    let apiInstanceStub: ApiInstanceStub;
    beforeAll(async () => {
        // needed for dependency injection.
        const container = await Container.createApplication({ providers: [OpenCRXService] })
            .overwriteProvider({ provide: OpenCRXApiInstance, use: ApiInstanceStub })
            .boot();
        service = container.get(OpenCRXService);
        apiInstanceStub = container.get(OpenCRXApiInstance);
    });
    it("should retrieve all sale information of a salesman with a specific id, identifiable by the @href attribute", async () => {
        const returnValue = [
            {
                salesRep: { "@href": "dummy/url/open-crx-id" },
            },
            {
                salesRep: { "@href": "dummy/url/this-is-a-random-id" },
            },
        ];
        apiInstanceStub.httpClient.get.mockResolvedValue({ data: { objects: returnValue } });
        const actual = await service.getAllSalesOfSalesman("open-crx-id", "2020");
        expect(actual.length).toEqual(1);
    });
});

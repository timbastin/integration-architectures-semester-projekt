export default class OrangeHRMServiceStub {
    addBonusSalary = jest.fn();

    getAllSalesman = jest.fn();

    getEmployeeDetailInformation = jest.fn();

    getOneSalesman = jest.fn();

    getSalesInformationOfSalesman = jest.fn();
}

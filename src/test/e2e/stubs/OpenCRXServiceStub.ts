export default class OpenCRXServiceStub {
    getAllSalesOfSalesman = jest.fn();

    getAllSalesmanFromOpenCRX = jest.fn();

    getClient = jest.fn();

    getOpenCRXAggregatedInformationForSalesman = jest.fn();

    getPositionsOfSale = jest.fn();

    getProduct = jest.fn();

    getSalesmanFromOpenCRX = jest.fn();

    getSalesmanFromOpenCRXByOrangeHRMId = jest.fn();
}

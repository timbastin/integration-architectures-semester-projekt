export default class ApiInstanceStub {
    constructor() {}
    public httpClient = {
        get: jest.fn(),
        post: jest.fn(),
        put: jest.fn(),
        patch: jest.fn(),
        delete: jest.fn(),
    };
}

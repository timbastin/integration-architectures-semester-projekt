export default class IdMappingServiceStub {
    create = jest.fn();

    delete = jest.fn();

    findOne = jest.fn();
}

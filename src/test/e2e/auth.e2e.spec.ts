import { App } from "../../App";
import Container from "../../lib/Container";
import express from "express";
import request from "supertest";
import OrangeHRMApiInstance from "../../OrangeHRMApiInstance";
import { Department } from "../../models/User";
import TestUtils from "../TestUtils";

describe("Auth e2e testing suite", () => {
    let superTest: request.SuperTest<request.Test>;
    beforeAll(async () => {
        const e = express();
        await Container.createApplication(App, e, true)
            // just a stupid mock, to avoid the application bootstrap event..
            .overwriteProvider({ provide: OrangeHRMApiInstance, use: class {} })
            .boot();
        superTest = request(e);
    });

    it("should return a 403 response, if the user is not found in the database, when logging in", (done) => {
        superTest
            .post("/auth/login")
            .send({ username: `${Math.random().toString(36).substring(7)}does-not-exist@web.com`, password: "secret" })
            .end((err, res) => {
                expect(res.status).toEqual(403);
                done();
            });
    });

    it("should be possible to register a new user", (done) => {
        superTest
            .post("/auth/register")
            .send({ username: `${Math.random().toString(36).substring(7)}@gmail.com`, password: "secret" })
            .end((err, res) => {
                expect(res.status).toEqual(201);
                expect(res.body.accessToken).toBeDefined();
                done();
            });
    });

    describe("with registered user", () => {
        let accessToken: string;
        const username = `${Math.random().toString(36).substring(7)}@gmail.com`;
        beforeAll(async () => {
            function fn(): Promise<{ accessToken: string }> {
                return new Promise((resolve) => {
                    superTest
                        .post("/auth/register")
                        .send({ username, department: Department.Sales, password: "secret" })
                        .end((err, res) => {
                            resolve(res.body);
                        });
                });
            }
            accessToken = (await fn()).accessToken;
        });
        it("should return a 400 response, when trying to register with an already registered username address.", (done) => {
            superTest
                .post("/auth/register")
                .send({ username, password: "secret" })
                .end((err, res) => {
                    expect(res.status).toEqual(400);
                    done();
                });
        });
        it("should return a signed token, when the username and password do match", (done) => {
            superTest
                .post("/auth/login")
                .send({ username, password: "secret" })
                .end((err, res) => {
                    expect(res.status).toEqual(201);
                    expect(res.body.accessToken).toBeDefined();
                    done();
                });
        });
        it("should return the department and the id of a user", (done) => {
            superTest
                .get("/auth")
                .set(...TestUtils.authHeader({ accessToken }))
                .end((err, res) => {
                    expect(res.status).toEqual(200);
                    console.log(res.body);
                    expect(res.body).toMatchObject({
                        department: Department.Sales,
                        _id: expect.any(String),
                        username: expect.any(String),
                    });
                    done();
                });
        });
    });
});

import { IEvaluationRecordDTO } from "../../dto";

const exampleEvaluationRecord: {
    year: string;
    bonus: null;
    id: string;
    orderEvaluation: {
        HooverGo: {
            clientRanking: number;
            quantity: number;
            clientName: string;
            bonus: null;
            comment: null;
            id: string;
            bonusId: null;
        }[];
        HooverClean: {
            clientRanking: number;
            quantity: number;
            clientName: string;
            bonus: null;
            comment: null;
            id: string;
            bonusId: null;
        }[];
    };
    department: string;
    socialEvaluation: (
        | { kpi: string; bonus: null; targetValue: string; actualValue: null; comment: null; id: string; bonusId: null }
        | { kpi: string; bonus: null; targetValue: string; actualValue: null; comment: null; id: string; bonusId: null }
        | { kpi: string; bonus: null; targetValue: string; actualValue: null; comment: null; id: string; bonusId: null }
        | { kpi: string; bonus: null; targetValue: string; actualValue: null; comment: null; id: string; bonusId: null }
        | { kpi: string; bonus: null; targetValue: string; actualValue: null; comment: null; id: string; bonusId: null }
    )[];
    remarks: string;
    isApprovedByCEO: boolean;
    sid: string;
} = {
    year: "2020",
    department: "Sales",
    isApprovedByCEO: false,
    id: "6012f45bea12beb375aadf9f",
    remarks: "",
    bonus: null,
    orderEvaluation: {
        HooverClean: [
            {
                id: "3CZN0GINLXPT60EBHQA5MAZ7J",
                clientName: "Telekom AG",
                clientRanking: 1,
                quantity: 10,
                bonusId: null,
                bonus: null,
                comment: null,
            },
        ],
        HooverGo: [
            {
                id: "3N1IOFZVIDZAI0EBHQA5MAZ7J",
                clientName: "Telekom AG",
                clientRanking: 1,
                quantity: 15,
                bonusId: null,
                bonus: null,
                comment: null,
            },
        ],
    },
    sid: "90124",
    socialEvaluation: [
        {
            kpi: "Integrity to Company",
            targetValue: "4",
            id: "6012d85c5844b092ea4c24f4",
            actualValue: null,
            comment: null,
            bonusId: null,
            bonus: null,
        },
        {
            kpi: "Leadership Competence",
            targetValue: "4",
            id: "6012d85c5844b092ea4c24f0",
            actualValue: null,
            comment: null,
            bonusId: null,
            bonus: null,
        },
        {
            kpi: "Communication Skills",
            targetValue: "4",
            id: "6012d85c5844b092ea4c24f3",
            actualValue: null,
            comment: null,
            bonusId: null,
            bonus: null,
        },
        {
            kpi: "Attitude towards Client",
            targetValue: "4",
            id: "6012d85c5844b092ea4c24f2",
            actualValue: null,
            comment: null,
            bonusId: null,
            bonus: null,
        },
        {
            kpi: "Openness to Employee",
            targetValue: "4",
            id: "6012d85c5844b092ea4c24f1",
            actualValue: null,
            comment: null,
            bonusId: null,
            bonus: null,
        },
    ],
};

export default exampleEvaluationRecord;

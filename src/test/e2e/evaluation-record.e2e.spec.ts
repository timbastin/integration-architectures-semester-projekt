import { App } from "../../App";
import Container from "../../lib/Container";
import express from "express";
import request from "supertest";
import OrangeHRMApiInstance from "../../OrangeHRMApiInstance";
import TestUtils from "../TestUtils";
import ApiInstanceStub from "./stubs/ApiInstanceStub";
import exampleEvaluationRecord from "./testData";
import { IEvaluationRecordDTO } from "../../dto";

describe("EvaluationRecord e2e testing suite", () => {
    let superTest: request.SuperTest<request.Test>;
    let token: { accessToken: string };
    beforeAll(async () => {
        const e = express();
        await Container.createApplication({ ...App, port: 3001 }, e, true)
            // just a stupid mock, to avoid the application bootstrap event..
            .overwriteProvider({ provide: OrangeHRMApiInstance, use: ApiInstanceStub })
            .boot();
        superTest = request(e);
        const utils = new TestUtils(superTest);
        token = await utils.login();
    });

    it("should correctly populate an already created evaluation record, with the stored boni", (done) => {
        superTest
            .get("/evaluation-records/90124/6012f45bea12beb375aadf9f")
            .set(...TestUtils.authHeader(token))
            .end((err, res) => {
                expect(res.status).toEqual(200);
                console.log(JSON.stringify(res.body));
                done();
            });
    });

    it("should create a new orderBonus, if one order bonus gets changed", (done) => {
        const data = { ...exampleEvaluationRecord };
        const [key] = Object.keys(data.orderEvaluation);
        data.orderEvaluation[key][0].bonus = 500;
        data.orderEvaluation[key][0].comment = "Good job";
        superTest
            .put("/evaluation-records/6012f45bea12beb375aadf9f")
            .send(data)
            .set(...TestUtils.authHeader(token))
            .end((err, res) => {
                expect(res.status).toEqual(200);
                // lets fetch the evaluation record again, and have a look, if it is updated
                superTest
                    .get("/evaluation-records/90124/6012f45bea12beb375aadf9f")
                    .set(...TestUtils.authHeader(token))
                    .end((err, res) => {
                        expect(res.status).toEqual(200);
                        const response: IEvaluationRecordDTO = res.body;
                        expect(response.orderEvaluation[key][0].bonus).toEqual(500);
                        expect(response.orderEvaluation[key][0].comment).toEqual("Good job");
                        done();
                    });
            });
    });

    it("should create a new kpiBonus, if one kpi bonus gets changed", (done) => {
        const data = { ...exampleEvaluationRecord };
        data.socialEvaluation[0].bonus = 200;
        data.socialEvaluation[0].comment = "Nice guy";
        superTest
            .put("/evaluation-records/6012f45bea12beb375aadf9f")
            .send(data)
            .set(...TestUtils.authHeader(token))
            .end((err, res) => {
                expect(res.status).toEqual(200);
                // lets fetch the evaluation record again, and have a look, if it is updated
                superTest
                    .get("/evaluation-records/90124/6012f45bea12beb375aadf9f")
                    .set(...TestUtils.authHeader(token))
                    .end((err, res) => {
                        expect(res.status).toEqual(200);
                        const response: IEvaluationRecordDTO = res.body;
                        expect(response.socialEvaluation[0].bonus).toEqual(200);
                        expect(response.socialEvaluation[0].comment).toEqual("Nice guy");
                        done();
                    });
            });
    });
});

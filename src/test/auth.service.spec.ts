import AuthService from "../services/AuthService";
import Container from "../lib/Container";
import { Department } from "../models/User";

describe("AuthService testing suite", () => {
    let service: AuthService;
    let app: Container;
    beforeAll(async () => {
        app = await Container.createApplication({ providers: [AuthService] }).boot();
        service = app.get(AuthService);
    });
    it("should correctly sign and verify a token", async () => {
        const payload = {
            username: "max",
            id: "5-5-5",
            department: Department.Sales,
        };
        expect(await service.verifyToken(await service.signToken(payload))).toMatchObject(payload);
    });
});
